#!/usr/local/bin/python
# ysrivastava
'''This is the top-level, standalone module for the game-library CLI app.'''

from __future__ import print_function
from collections import OrderedDict
import getpass
import argparse
import subprocess
import os, sys
import time
import mysql.connector
import sshtunnel

#############################################################
# GLOBAL SUBROUTINES
#############################################################

def init_options():
    '''Create and return parser object'''
    argparser = argparse.ArgumentParser()

    argparser.add_argument('-l', '-list', '--list', default=True, action="store_true", help="List all games in database")
    argparser.add_argument('-g', '-genres', '--genres', default=False, action="store_true", help="List valid genres for a given mode")
    argparser.add_argument('-a', '-add', '--add', default=None, const='prompt_user', action="store", nargs='?', help="Add game into database")
    argparser.add_argument('-m', '-mode', '--mode', default=None, const='board', action="store", nargs='?', help="Query mode (board or video)")
    return argparser

def fatal(conn,status):
    '''Generic error function which closes any open connection'''
    if conn:
        conn.close()
    sys.exit(status)

def create_game(opt,conn):
    '''Create and return game object'''
    if opt.mode == "video":
        gameobj = VideoGame(conn)
    else:
        gameobj = BoardGame(conn)

    return gameobj

def process_aux_modes(conn,opt,game):
    if opt.genres:
        print("\n".join(game.get_valid_genres()))
        fatal(conn,0)

#############################################################
# CLASSES
#############################################################

class Game:
    '''Base class for Game objects'''

    def __init__(self,conn):
        '''Game base class constructor'''
        self.table = ""
        self.conn = conn

        cursor = conn.get_cursor()

         # Create query object for listing/adding games
        self.q = Query(cursor,conn)

    def add(self,filename):
        '''Add game to DB'''
        if filename == "prompt_user":
            data = self.get_game_info()
            if self.check_for_existing_data(data):
                fatal(self.conn,4)

            self.q.add(self.table,self.get_val_str(data))
        else:
            game_info = self.import_file(filename)
            for entry in game_info:
                fields = entry.rstrip().split('|')
                data = self.get_game_info(fields)

                if not self.check_for_existing_data(data):
                    self.q.add(self.table,self.get_val_str(data))

    def import_file(self,filename):
        '''Import CSV file'''
        try:
            f = open(filename, 'r')
        except IOError:
            print("Fatal error: unable to open file {0}".format(filename))
            fatal(None,2)

        data = f.readlines()
        f.close()

        return data

    def check_for_existing_data(self,data):
        '''Check to see if specified data already exists in database'''
        cmd = 'select * from {0} where name = "{1}"'.format(self.table, data['name'])
        rows = self.q.run(cmd)

        if rows:
            if rows[0][1] == data['name']:
                print("'" + data['name'] + "' already exists in the database")
                return True

        return False

    def get_max_field_widths(self,rows):
        '''Get the max field width for each row'''
        widths = [0] * len(rows[0])

        for row in rows:
            fields = list(row)
            fields.pop(0) # Ignore the primary key

            for i in range(len(fields)):
                width = len(str(fields[i]))
                widths[i] = max(len(str(fields[i])),widths[i])

        return widths

    def get_game_info(self):
        '''Virtual method'''
        raise NotImplementedError()

    def get_val_str(self,data):
        '''Virtual method'''
        raise NotImplementedError()


class VideoGame(Game):
    '''Class for adding/querying video games'''
    def __init__(self,conn):
        '''Constructor'''
        Game.__init__(self,conn)
        self.table = 'video_games'
        self.num_rows = 4
        self.col_space = 3

    def get_valid_genres(self):
        genres = [
                    'Action',
                    'Adventure',
                    'Beat \'em up',
                    'Casual',
                    'Fighting',
                    'FPS',
                    'Platformer',
                    'Puzzle',
                    'Racing',
                    'Roguelike',
                    'RPG',
                    'Run and gun',
                    'Shoot \'em up',
                    'Simulation',
                    'Sports',
                    'Strategy',
                 ]

        return genres

    def list(self):
        '''List video games in database'''
        rows = self.q.list(self.table)

        widths = self.get_max_field_widths(rows)

        header_names = ['|Name|', '|System|', '|Year|', '|Genre|']
        indx = 0
        for name in header_names:
            print("{:<{}}".format(name,widths[indx]+self.col_space), end='')
            indx+=1
        print("")

        for row in rows:
             # Print out fields, add extra column space to make things look nicer
            indx = 0
            fields = list(row)
            fields.pop(0) # Ignore the primary key
            for field in fields:
                print("{:<{}}".format(field,widths[indx]+self.col_space), end='')
                indx+=1
            print("")

    def get_game_info(self,game_data=None):
        '''Get details of to-be-added game from user'''
        game_info = OrderedDict([
                        ("Name of game?" , 'name'),
                        ("System/console?" , 'system'),
                        ("Year released?" , 'year'),
                        ("Genre(s)? (comma-delimited)" , 'genre'),
                    ])

        func_for_info = {
                            'name'   : self.process_game_input,
                            'system' : self.process_system_input,
                            'year'   : self.process_year_input,
                            'genre'  : self.process_genre_input,
                        }

        game_attrs = {}

        if game_data is not None:
            game_data_copy = list(game_data)

        for question in game_info:
            if game_data == None:
                response = input(question + " ").strip()
                output = func_for_info[game_info[question]](response)
            else:
                try:
                    output = game_data.pop(0)
                except Exception as e:
                    print("Fatal error: Not enough fields: [{0}]".format(game_data_copy))
                    fatal(self.conn,3)

            game_attrs[game_info[question]] = output

        return game_attrs

    def process_game_input(self,value):
        '''Dummy handle, nothing to process right now'''
        return value

    def process_system_input(self,value):
        '''Validate system input'''
        valid_systems = [
                            'Neo Geo MVS',
                            'NES',
                            'Nintendo 64',
                            'PC',
                            'PC Engine',
                            'PS2',
                            'PS3',
                            'SNES',
                            'Sega Genesis',
                            'Wii',
                            'Wii U',
                            'Vectrex',
                        ]

        for system in valid_systems:
            if system.lower() == value.lower():
                return system

        print("\nInvalid system entered. Please select one of the following:")

        for i in range(0, len(valid_systems)):
            print("{0}. {1}".format(i+1, valid_systems[i]))

        selection = input("> ").strip()
        if selection.isdigit() and int(selection) < len(valid_systems):
            selection = valid_systems[int(selection)]

        value = self.process_system_input(selection)

        print("\n"),
        return value

    def process_year_input(self,value):
        '''Validate passed-in year'''
        if value.isdigit() and len(value) == 4:
            return value

        print("Invalid year entered. Please try again.")
        selection = input("> ").strip()

        value = self.process_year_input(selection)
        return value

    def process_genre_input(self,value):
        '''Validated passed-in genre(s)'''
        valid_genres = self.get_valid_genres()
        found_invalid_genre = False

        try:
            genres = value.split(',')
        except ValueError:
            genres.append(value)

         # Spin through the supplied genres and the valid genre list
         # to figure out what's valid and invalid
        for i in range(0, len(genres)):
            found_match = False
            for valid_choice in valid_genres:
                if genres[i].lower() == valid_choice.lower():
                    genres[i] = valid_choice
                    found_match = True
                    break

            if not found_match:
                print("'{0}' is not a valid genre.".format(genres[i]))
                found_invalid_genre = True

        if not found_invalid_genre:
            genre_list = ','.join(genres)
            return genre_list

        print("\nPlease enter a new comma-delimited genre list from the following:")

        for option in valid_genres:
            print(option)

        selection = input("> ").strip()

        value = self.process_genre_input(selection)
        return value

    def get_val_str(self,data):
        '''Return SQL string for data'''
        return '(name,system,year,genre) VALUES ("{0}","{1}","{2}","{3}")'.format(data['name'],data['system'],data['year'],data['genre'])

class BoardGame(Game):
    '''Class for adding/querying board games'''
    def __init__(self,conn):
        '''Constructor'''
        Game.__init__(self,conn)
        self.table = 'board_games'
        self.num_rows = 6
        self.col_space = 12

    def get_valid_genres(self):
        genres = [
                    'Area Control',
                    'Bidding',
                    'Bluffing',
                    'Card Drafting',
                    'City Building',
                    'Cooperative',
                    'Deckbuilding',
                    'Deduction',
                    'Dexterity',
                    'Dice',
                    'Economic',
                    'Exploration',
                    'Grid Movement',
                    'Hand Management',
                    'Party',
                    'Programmable Movement',
                    'Route Connection',
                    'Storytelling',
                    'Tile Placement',
                    'Traitor',
                    'Worker Placement',
                 ]

        return genres                  

    def list(self):
        '''List board games in database'''
        rows = self.q.list(self.table)

        widths = self.get_max_field_widths(rows)
        header_names = ['|Name|', '|Year|', '|Genre|', '|Min Players|', '|Max Players|', '|Playing Time|']
        indx = 0
        for name in header_names:
            print("{:<{}}".format(name, widths[indx]+self.col_space), end='')
            indx+=1
        print("")

        for row in rows:
             # Print out fields, add extra column space to make things look nicer
            indx = 0
            fields = list(row)
            fields.pop(0) # Ignore the primary key
            for field in fields:
                print("{:<{}}".format(field, widths[indx]+self.col_space), end='')
                indx+=1
            print("")


    def get_game_info(self,game_data=None):
        '''Get details of to-be-added game from user'''
        game_info = OrderedDict([
                        ("Name of game?" , 'name'),
                        ("Year released?" , 'year'),
                        ("Genre(s)? (comma-delimited)" , 'genre'),
                        ("Min # of Players?" , 'min_players'),
                        ("Max # of Players?" , 'max_players'),
                        ("Playing Time (Minutes)?" , 'playtime'),
                    ])

        func_for_info = {
                            'name'   : self.process_game_input,
                            'year'   : self.process_year_input,
                            'min_players' : self.process_min_players_input,
                            'max_players' : self.process_max_players_input,
                            'playtime' : self.process_playtime_input,
                            'genre'  : self.process_genre_input,
                        }

        game_attrs = {}

        if game_data is not None:
            game_data_copy = list(game_data)

        for question in game_info:
            if game_data is None:
                response = input(question + " ").strip()
                output = func_for_info[game_info[question]](response)
            else:
                try:
                    output = game_data.pop(0)
                except Exception as e:
                    print("Fatal error: Not enough fields: [{0}]".format(game_data_copy))
                    fatal(self.conn,3)

            game_attrs[game_info[question]] = output

        return game_attrs

    def process_game_input(self,value):
        '''Dummy handle, nothing to process right now'''
        return value

    def process_year_input(self,value):
        '''Validate passed-in year'''
        if value.isdigit() and len(value) == 4:
            return value

        print("Invalid year entered. Please try again.")
        selection = input("> ").strip()

        value = self.process_year_input(selection)
        return value

    def process_min_players_input(self,value):
        '''Validate passed-in number of players'''
        if value.isdigit():
            return value

        print("Invalid # of players entered. Please try again.")
        selection = input("> ").strip()

        value = self.process_min_players_input(selection)
        return value

    def process_max_players_input(self,value):
        '''Validate passed-in number of players'''
        if value.isdigit():
            return value

        print("Invalid # of players entered. Please try again.")
        selection = input("> ").strip()

        value = self.process_max_players_input(selection)
        return value

    def process_playtime_input(self,value):
        '''Validate passed-in time'''
        if value.isdigit():
            return value

        print("Invalid playing time entered. Please try again.")
        selection = input("> ").strip()

        value = self.process_playtime_input(selection)
        return value

    def process_genre_input(self,value):
        '''Validated passed-in genre(s)'''
        valid_genres = self.get_valid_genres()
        found_invalid_genre = False

        try:
            genres = value.split(',')
        except ValueError:
            genres.append(value)

         # Spin through the supplied genres and the valid genre list
         # to figure out what's valid and invalid
        for i in range(0, len(genres)):
            found_match = False
            for valid_choice in valid_genres:
                if genres[i].lower() == valid_choice.lower():
                    genres[i] = valid_choice
                    found_match = True
                    break

            if found_match == False:
                print("'{0}' is not a valid genre.".format(genres[i]))
                found_invalid_genre = True

        if found_invalid_genre == False:
            genre_list = ','.join(genres)
            return genre_list

        print("\nPlease enter a new comma-delimited genre list from the following:")

        for option in valid_genres:
            print(option)

        selection = input("> ").strip()

        value = self.process_genre_input(selection)
        return value

    def get_val_str(self,data):
        '''Return SQL string for data'''
        return "(name,year,genre,min_players,max_players,playtime) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')".format(data['name'],data['year'],data['genre'],data['min_players'],data['max_players'],data['playtime'])

class Connection:
    '''Class for connection methods'''
    def __init__(self):
        '''Constructor'''
        self.is_server = False
        if self.get_hostname() == b'macklin':
            self.is_server = True

        self.cur = None
        self.srv = None
        self.db = None

    def open(self,is_admin):
        '''Open connection to server'''
        self.srv = self.create_ssh_tunnel()

        usr = 'gamenight'
        pw = 'gamenight42'

        if is_admin:
            usr = input("User: ").strip()
            pw = getpass.getpass("Password: ")
            print('\n')

        self.db = mysql.connector.connect(host='127.0.0.1',database='games',user='{0}'.format(usr),password='{0}'.format(pw),use_pure=True)
        self.cur = self.db.cursor()

    def close(self):
        '''Close connection to server'''
        if self.db is not None:
            self.db.commit()
            self.db.close()

        self.stop_ssh_tunnel(self.srv)

    def get_cursor(self):
        '''Return connection cursor'''
        return self.cur

    def create_ssh_tunnel(self):
        '''Create SSH tunnel to host'''
         # If we're actually on the server, don't create an SSH tunnel
        if self.is_server:
            return None

        server = sshtunnel.SSHTunnelForwarder(
            'noiseroom.bounceme.net',
             ssh_username="gamedb",
             ssh_password="gamedb42",
             remote_bind_address=('127.0.0.1', 3306),
             local_bind_address=('127.0.0.1', 3306),
        )
        server.start()

        return server

    def stop_ssh_tunnel(self,server):
        '''Close SSH tunnel'''
         # If we're actually on the server, don't stop the SSH tunnel
        if self.is_server:
            return None

        server.stop()

    def get_hostname(self):
        '''Return hostname'''
        return subprocess.check_output(['hostname']).strip()

class Query:
    '''Class to handle SQL query methods'''
    def __init__(self,cur,conn):
        '''Constructor'''
        self.cur = cur
        self.conn = conn

    def list(self,table):
        '''List rows from table'''
        try:
            self.cur.execute("select * from " + table)
        except Exception as e:
            print("Fatal error: failed to query DB: [{0}]".format(e))
            fatal(self.conn,2)

        return self.cur.fetchall()

    def run(self,cmd):
        '''Run query'''
        try:
            self.cur.execute(cmd)
        except Exception as e:
            print("Fatal error: failed to run query: [{0}]".format(e))
            fatal(self.conn,2)

        return self.cur.fetchall()

    def add(self,table,val_str):
        '''Insert data into table'''
        try:
            self.cur.execute("insert into " + table + " " + val_str)
        except Exception as e:
            print("Fatal error: failed to run query: [{0}]".format(e))
            fatal(self.conn,2)

#############################################################
# MAIN
#############################################################

if __name__ == "__main__":

    parser = init_options()
    cli_opt = parser.parse_args()

     # Create SSH tunnel to MySQL db on server
    connection = Connection()
    try:
        connection.open(cli_opt.add)
    except (sshtunnel.BaseSSHTunnelForwarderError,mysql.connector.Error) as e:
        print("Failed to establish connection: [{0}]".format(e))
        fatal(connection,1)

    game = create_game(cli_opt,connection)

    process_aux_modes(connection,cli_opt,game)
    if cli_opt.add:
        game.add(cli_opt.add)
    elif cli_opt.list:
        game.list()

    connection.close()
