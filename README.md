# game-night.py

## Description

The following README details the setup/installation process for each FreeBSD and Windows 10.

## Prerequisites

    python >=3
    pip

## Installation

Run [repo]/[os]/install.bat to install sshtunnel and mysql-connector-python via pip.

These may be installed manually, as well.

## Usage

The following databases may be accessed from the game-night SQL server via this script:

    video_games
    board_games

These are queried as such (the --list or -l option is used by default):

    python game-library.py --mode=video
    python game-library.py --mode=board

    python game-library.py -m=video
    python game-library.py -m=board
